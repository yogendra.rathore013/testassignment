# TestAssignment
This is a BDD Cucumber automation framework.
We are using extent reports for reporting with screenshots and backing up last execution
results for future reference.


## Getting started
To Run Test Scenario , Right click on the TestAssignment folder >> Run As >> Junit Test



## Reports 
You can see the reports as CurrentTestResults folder and Logs in logs folder.
