Feature: This feature verifies Payment from one user to another user

	Scenario:  Verify payment from user to another user
	Given I am on home page
	Then I verify I am on home page
	When I generate a random user using random user API	
	When I register a new user while checking user is not already registered
	Then I fetch account number of the user
	When I logout
	Then I verify logged out successfully
	When I generate a random user using random user API
	When I register a new user while checking user is not already registered
	When I go to Bill Pay page
	Then I verify I am on Bill Pay Page
	When I send amount 20 to another user
	Then I verify amount 20 send to user successfully
  Then I assert all
