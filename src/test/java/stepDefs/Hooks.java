package stepDefs;

import Base.Base;
import Constants.Paths;
import Util.JAVAUtil;
import Util.ReadPropertiesFile;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class Hooks extends Base {

	public static boolean isFirstTestCase = true;

	@Before
	public void beforeScenario(Scenario sc) throws Exception {

		System.out.println("<----**************** Scenario Start ****************************---->");
		System.out.println("Scenario Name :- " + sc.getName());

		// start extent report
		startExtentReport();
		createTestForExtent(sc.getName());

		// intialize properties object
		new ReadPropertiesFile();

		// clear soft assertion list
		clearSoftAssertAndScreenshotsList();

		if (isFirstTestCase) {
			isFirstTestCase = false;
			JAVAUtil.deleteFolderAtGivenLocation(Paths.PATH_CURRENT_TEST_RESULTS, "Screenshots");
		}
	}

	@After
	public void afterScneario(Scenario sc) {
		System.out.println("<----****************** Scenario End **************************---->");

		if (props != null)
			props.clear();

		createStatusForExtentReport(sc.getStatus().toString());

		// to write or update test information to reporter
		flushExtentReport();

		// back up test results for future reference.
		JAVAUtil.backupTestReportAndLogs();

		// quit driver
		 driver.close();

	}
}
