package stepDefs;

public class Locators {

 
	/**
	 * This class contains xPaths which will be used in framework
	 */

	// Home Page
	public static final String XPATH_IMG_HOME_LOGO= "//img[@title='ParaBank']";
	public static final String XPATH_LNK_REGISTER= "//a[text()='Register']";
	
	//Register new user
	public static final String XPATH_TXT_FIRST= "//input[@id='customer.firstName']";
	public static final String XPATH_TXT_LAST= "//input[@id='customer.lastName']";
	public static final String XPATH_TXT_STREET= "//input[@id='customer.address.street']";
	public static final String XPATH_TXT_CITY= "//input[@id='customer.address.city']";
	public static final String XPATH_TXT_STATE= "//input[@id='customer.address.state']";
	public static final String XPATH_TXT_ZIPCODE= "//input[@id='customer.address.zipCode']";
	public static final String XPATH_TXT_PHONENUMBER= "//input[@id='customer.phoneNumber']";
	public static final String XPATH_TXT_SSN= "//input[@id='customer.ssn']";
	public static final String XPATH_TXT_USERNAME= "//input[@id='customer.username']";
	public static final String XPATH_TXT_PASSWORD= "//input[@id='customer.password']";
	public static final String XPATH_TXT_CONFIRM_PASSWORD= "//input[@id='repeatedPassword']";

	public static final String XPATH_BTN_REGISTER= "//input[@value='Register']";
	
	public static final String XPATH_ERR_USER_ALREADY_EXITS= "//span[text()='This username already exists.']";

	// logged in app
	public static final String XPATH_LNK_LOGOUT="//div[@id='leftPanel']//a[text()='Log Out']";
	public static final String XPATH_LNK_BILL_PAY="//div[@id='leftPanel']//a[text()='Bill Pay']";
	public static final String XPATH_LNK_ACCOUNT_OVERVIEW="//div[@id='leftPanel']//a[text()='Accounts Overview']";
	public static final String XPATH_DD_ACCOUNTNUMBER="//select[@name='fromAccountId']";
	 
	
	// Bill pay page
	
	public static final String XPATH_TXT_PAYEE_NAME= "//input[@name='payee.name']";
	public static final String XPATH_TXT_PAYEE_ADDRESS= "//input[@name='payee.address.street']";
	public static final String XPATH_TXT_PAYEE_CITY= "//input[@name='payee.address.city']";
	public static final String XPATH_TXT_PAYEE_STATE= "//input[@name='payee.address.state']";
	public static final String XPATH_TXT_PAYEE_POSTCODE= "//input[@name='payee.address.zipCode']";
	public static final String XPATH_TXT_PAYEE_PHONE= "//input[@name='payee.phoneNumber']";
	public static final String XPATH_TXT_PAYEE_ACCOUNTNUMBER= "//input[@name='payee.accountNumber']";
	public static final String XPATH_TXT_PAYEE_VERIFY_ACCOUNT= "//input[@name='verifyAccount']";
	public static final String XPATH_TXT_PAYEE_AMOUNT= "//input[@name='amount']";
	public static final String XPATH_BTN_PAYEE_SEND_PAYMENT= "//input[@value='Send Payment']";
	
	
	// transcation complete
	public static final String XPATH_LBL_BILL_PAYMENT_COMPLETE= "//h1[text()='Bill Payment Complete']";
	public static final String XPATH_LBL_BILL_PAYMENT_COMPLETE_PAYEENAME= "//h1[text()='Bill Payment Complete']/../p//span[@id='payeeName']";
	public static final String XPATH_LBL_BILL_PAYMENT_COMPLETE_AMOUNT= "//h1[text()='Bill Payment Complete']/../p//span[@id='amount']";
	 
	
	//account over view
	public static final String XPATH_LNK_ACCOUNT_NUMBER="//table[@id='accountTable']//a";
	
	public static final String XPATH_LBL_ACCOUNT_DETAILS= "//h1[text()='Account Details']";
	public static final String XPATH_LNK_TRANSCATION_TABLE="//table[@id='transactionTable']//a";
	public static final String XPATH_LBL_TRANSCATION_DETAILS= "//h1[text()='Transaction Details']";
	public static final String XPATH_TABLE_TD= "//table//td";
	
	
	//login
	public static final String XPATH_TXT_LOGIN_USERNAME= "//input[@name='username']";
	public static final String XPATH_TXT_LOGIN_PASSWORD= "//input[@name='password']";
	public static final String XPATH_BTN_LOG_IN= "//input[@value='Log In']";
	
	
}
