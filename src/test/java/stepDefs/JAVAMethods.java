package stepDefs;

import Base.Base;
import Constants.Constants;
import Util.APIJSONParser;
import Util.JAVAUtil;
import Util.SeleniumUtil;

public class JAVAMethods extends Base {
/**
 * @author yogendrarathore01
 * @throws Exception
 */
	public static void registerNewUswer() throws Exception {

		SeleniumUtil.enterText(Locators.XPATH_TXT_FIRST, APIJSONParser.first);
		SeleniumUtil.enterText(Locators.XPATH_TXT_LAST, APIJSONParser.last);
		SeleniumUtil.enterText(Locators.XPATH_TXT_STREET,
				APIJSONParser.street_number + " " + APIJSONParser.street_name);
		SeleniumUtil.enterText(Locators.XPATH_TXT_CITY, APIJSONParser.city);
		SeleniumUtil.enterText(Locators.XPATH_TXT_STATE, APIJSONParser.state);
		SeleniumUtil.enterText(Locators.XPATH_TXT_ZIPCODE, APIJSONParser.postcode);
		SeleniumUtil.enterText(Locators.XPATH_TXT_PHONENUMBER, APIJSONParser.phone);
		SeleniumUtil.enterText(Locators.XPATH_TXT_SSN, JAVAUtil.getRandomNumberLessThanGivenNumber(10000));
		SeleniumUtil.enterText(Locators.XPATH_TXT_USERNAME, APIJSONParser.username);
		SeleniumUtil.enterText(Locators.XPATH_TXT_PASSWORD, APIJSONParser.password);
		SeleniumUtil.enterText(Locators.XPATH_TXT_CONFIRM_PASSWORD, APIJSONParser.password);

		SeleniumUtil.click(Locators.XPATH_BTN_REGISTER);
	}
/**
 * @author yogendrarathore01
 * @param int1
 * @throws Exception
 */
	public static void sendMoney(Integer int1) throws Exception {

		SeleniumUtil.enterText(Locators.XPATH_TXT_PAYEE_NAME, testData.get(Constants.PAYEE_NAME + "_1"));
		SeleniumUtil.enterText(Locators.XPATH_TXT_PAYEE_ADDRESS, testData.get(Constants.API_STREET + "_1"));
		SeleniumUtil.enterText(Locators.XPATH_TXT_PAYEE_CITY, testData.get(Constants.API_CITY + "_1"));
		SeleniumUtil.enterText(Locators.XPATH_TXT_PAYEE_STATE, testData.get(Constants.API_STATE + "_1"));
		SeleniumUtil.enterText(Locators.XPATH_TXT_PAYEE_POSTCODE, testData.get(Constants.API_POSTCODE + "_1"));
		SeleniumUtil.enterText(Locators.XPATH_TXT_PAYEE_PHONE, testData.get(Constants.API_PHONE + "_1"));
		SeleniumUtil.enterText(Locators.XPATH_TXT_PAYEE_ACCOUNTNUMBER, testData.get(Constants.ACCOUNT_NUMBER + "_1"));
		SeleniumUtil.enterText(Locators.XPATH_TXT_PAYEE_VERIFY_ACCOUNT, testData.get(Constants.ACCOUNT_NUMBER + "_1"));
		SeleniumUtil.enterText(Locators.XPATH_TXT_PAYEE_AMOUNT, String.valueOf(int1));
		SeleniumUtil.click(Locators.XPATH_BTN_PAYEE_SEND_PAYMENT);

	}
/**
 * @author yogendrarathore01
 * @param userName
 * @param password
 * @throws Exception
 */
	public static void login(String userName, String password) throws Exception {
		 
		SeleniumUtil.enterText(Locators.XPATH_TXT_LOGIN_USERNAME, userName);
		SeleniumUtil.enterText(Locators.XPATH_TXT_LOGIN_PASSWORD, password);
		SeleniumUtil.click(Locators.XPATH_BTN_LOG_IN);

	}
}
