package stepDefs;

import java.util.List;

import Base.Base;
import Constants.Constants;
import Util.APIJSONParser;
import Util.JAVAUtil;
import Util.SeleniumUtil;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDef extends Base {

	@Given("I am on home page")
	public void i_am_on_home_page() {
		try {
			// open home page
			openHomePage();
		} catch (Exception e) {

			softAssertAndTakeScreenshot("Exception occured->" + e.getMessage(), testcaseName, true);
			assertAll();

		}

	}

	@Then("I verify I am on home page")
	public void i_verify_i_am_on_home_page() {
		try {

			if (!SeleniumUtil.checkXapthIsDisplayed(Locators.XPATH_IMG_HOME_LOGO, 15))
				softAssertAndTakeScreenshot("Home Page is not displayed", testcaseName, true);
			else {
				addLogAndExtentReportMessage("Home Page is displayed successfully.");

			}
		} catch (Exception e) {
			softAssertAndTakeScreenshot("Exception occured->" + e.getMessage(), testcaseName, true);

			assertAll();
		}
	}

	@When("I generate a random user using random user API")
	public void i_generate_a_random_user_using_random_user_api() {
		try {
			APIJSONParser.setValuesForAPIVariables();
			addLogAndExtentReportMessage("API parsed successfully for new user data.");
		} catch (Exception e) {
			softAssertAndTakeScreenshot("Exception occured->" + e.getMessage(), testcaseName, true);
			assertAll();
		}
	}

	@When("I register a new user while checking user is not already registered")
	public void i_register_a_new_user_while_checking_user_is_not_already_registered() {
		try {
			int retry = 0;
			int maxRetry = Integer.parseInt(props.getProperty(Constants.CONFIG_MAX_RETRY));
			SeleniumUtil.click(Locators.XPATH_LNK_REGISTER);

			if (SeleniumUtil.checkXapthIsDisplayed(Locators.XPATH_TXT_FIRST, 15)) {
				// Register new user

				JAVAMethods.registerNewUswer();

				// check for user logged in
				if (SeleniumUtil.checkXapthIsDisplayed(Locators.XPATH_LNK_LOGOUT, 5)) {

					addLogAndExtentReportMessage("User registered successfully.");
				} else
					// check for error
					while (retry < maxRetry
							&& SeleniumUtil.checkXapthIsDisplayed(Locators.XPATH_ERR_USER_ALREADY_EXITS, 5)) {

						retry++;
						addLogAndExtentReportMessage(
								APIJSONParser.username + "--->User already registered successfully.");

						String temp = APIJSONParser.username + JAVAUtil.getRandomNumberLessThanGivenNumber(500);
						addLogAndExtentReportMessage("Append random digit and try to register again.-->" + temp);

						SeleniumUtil.enterText(Locators.XPATH_TXT_USERNAME, temp);
						SeleniumUtil.enterText(Locators.XPATH_TXT_PASSWORD, APIJSONParser.password);
						SeleniumUtil.enterText(Locators.XPATH_TXT_CONFIRM_PASSWORD, APIJSONParser.password);
						SeleniumUtil.click(Locators.XPATH_BTN_REGISTER);

						// check for user logged in
						if (SeleniumUtil.checkXapthIsDisplayed(Locators.XPATH_LNK_LOGOUT, 5)) {

							addLogAndExtentReportMessage("User registered successfully.");
							APIJSONParser.username = temp;
							testData.put(Constants.API_USERNAME + "_" + testDataIndex, temp);

						}

					}

				if (retry >= maxRetry) {
					testData.clear();
					testDataIndex--;
					APIJSONParser.setValuesForAPIVariables();
					i_register_a_new_user_while_checking_user_is_not_already_registered();
				}

			} else
				softAssertAndTakeScreenshot("Register Link is not working.", testcaseName, true);
		} catch (Exception e) {

			softAssertAndTakeScreenshot("Exception occured->" + e.getMessage(), testcaseName, true);
			assertAll();
		}
	}

	@Then("I fetch account number of the user")
	public void i_fetch_account_number_of_the_user() {
		try {
			SeleniumUtil.applicationWait(3000);
			SeleniumUtil.click(Locators.XPATH_LNK_BILL_PAY);
			SeleniumUtil.checkXapthIsDisplayed(Locators.XPATH_DD_ACCOUNTNUMBER, 5);

			String accountNumber = SeleniumUtil.getText(Locators.XPATH_DD_ACCOUNTNUMBER);
			addLogAndExtentReportMessage("Account number of first user -->>" + accountNumber);
			testData.put(Constants.ACCOUNT_NUMBER + "_" + testDataIndex, accountNumber);

		} catch (Exception e) {
			softAssertAndTakeScreenshot("Exception occured->" + e.getMessage(), testcaseName, true);
			assertAll();
		}
	}

	@When("I logout")
	public void i_logout() {
		try {
			SeleniumUtil.click(Locators.XPATH_LNK_LOGOUT);

		} catch (Exception e) {
			softAssertAndTakeScreenshot("Exception occured->" + e.getMessage(), testcaseName, true);
			assertAll();
		}
	}

	@Then("I verify logged out successfully")
	public void i_verify_logged_out_successfully() {

		try {
			SeleniumUtil.applicationWait(3000);
			if (SeleniumUtil.checkXapthIsDisplayed(Locators.XPATH_LNK_LOGOUT, 5))
				softAssertAndTakeScreenshot("User not logged out successfully.", testcaseName, true);
			else {
				addLogAndExtentReportMessage("User  logged out successfully.");

			}
		} catch (Exception e) {
			softAssertAndTakeScreenshot("Exception occured->" + e.getMessage(), testcaseName, true);
			assertAll();
		}
	}

	@When("I go to Bill Pay page")
	public void i_go_to_bill_pay_page() {
		try {
			SeleniumUtil.click(Locators.XPATH_LNK_BILL_PAY);

		} catch (Exception e) {
			softAssertAndTakeScreenshot("Exception occured->" + e.getMessage(), testcaseName, true);
			assertAll();
		}
	}

	@Then("I verify I am on Bill Pay Page")
	public void i_verify_i_am_on_bill_pay_page() {
		try {
			if (!SeleniumUtil.checkXapthIsDisplayed(Locators.XPATH_TXT_PAYEE_NAME, 15))
				softAssertAndTakeScreenshot("Bill pay page not displayed successfully.", testcaseName, true);
			else {
				addLogAndExtentReportMessage("Bill pay page displayed successfully.");

			}
		} catch (Exception e) {
			softAssertAndTakeScreenshot("Exception occured->" + e.getMessage(), testcaseName, true);
			assertAll();
		}
	}

	@When("I send amount {int} to another user")
	public void i_send_amount_to_another_user(Integer int1) {
		try {
			addLogAndExtentReportMessage("I attempt to send money -->" + int1);
			JAVAMethods.sendMoney(int1);

		} catch (Exception e) {
			softAssertAndTakeScreenshot("Exception occured->" + e.getMessage(), testcaseName, true);
			assertAll();
		}
	}

	@Then("I verify amount {int} send to user successfully")
	public void i_verify_amount_send_to_user_successfully(Integer int1) {
		try {

			if (!SeleniumUtil.checkXapthIsDisplayed(Locators.XPATH_LBL_BILL_PAYMENT_COMPLETE, 15))
				softAssertAndTakeScreenshot("Bill Payment Complete message not displayed successfully.", testcaseName,
						true);
			else {
				addLogAndExtentReportMessage("Bill Payment Complete message displayed successfully.");

				String payeeName = SeleniumUtil.getText(Locators.XPATH_LBL_BILL_PAYMENT_COMPLETE_PAYEENAME);
				String expected_payeeName = testData.get(Constants.PAYEE_NAME + "_1");

				String amount = SeleniumUtil.getText(Locators.XPATH_LBL_BILL_PAYMENT_COMPLETE_AMOUNT).replaceAll(".00",
						"");

				if (!payeeName.equals(expected_payeeName))
					softAssertAndTakeScreenshot("Payee Name not correct in Payment successfull message - Expected :"
							+ expected_payeeName + ", Actual:" + payeeName, testcaseName, true);
				else
					addLogAndExtentReportMessage("Payee Name correct in Payment successfull message. ");

				if (!amount.equals("$" + String.valueOf(int1)))
					softAssertAndTakeScreenshot("Amount not correct in Payment successfull message - Expected :|" + "$"
							+ String.valueOf(int1) + "|, Actual:|" + amount + "|", testcaseName, true);
				else
					addLogAndExtentReportMessage("Amount correct in Payment successfull message. ");

			}

		} catch (Exception e) {
			softAssertAndTakeScreenshot("Exception occured->" + e.getMessage(), testcaseName, true);
			assertAll();
		}
	}

	@Then("I verify transaction history for {int} debited and {int} credited for same transaction id")
	public void i_verify_transaction_history_for_debited_and_credited_for_same_transaction_id(Integer int1,
			Integer int2) {
		try {

			SeleniumUtil.applicationWait(3000);

			// click on account overview from left panel
			SeleniumUtil.click(Locators.XPATH_LNK_ACCOUNT_OVERVIEW);

			// verify user is on account over view page
			if (SeleniumUtil.checkXapthIsDisplayed(Locators.XPATH_LNK_ACCOUNT_NUMBER, 5)) {
				addLogAndExtentReportMessage("User is on account over view page. ");

				// click on account number link
				SeleniumUtil.click(Locators.XPATH_LNK_ACCOUNT_NUMBER);

				// verify user is on account details page
				if (SeleniumUtil.checkXapthIsDisplayed(Locators.XPATH_LBL_ACCOUNT_DETAILS, 5)) {
					addLogAndExtentReportMessage("User is on account details page. ");

					SeleniumUtil.click(Locators.XPATH_LNK_TRANSCATION_TABLE);

					if (SeleniumUtil.checkXapthIsDisplayed(Locators.XPATH_LBL_TRANSCATION_DETAILS, 5)) {
						addLogAndExtentReportMessage("User is on transaction details page. ");

						// get actual data
						List<String> tempTextList = SeleniumUtil.getTextsFromAllElements(Locators.XPATH_TABLE_TD);

						String transaction_ID = tempTextList.get(1);

						String name_actual = tempTextList.get(5).replaceAll("Bill Payment to ", "");
						String transaction_type_actual = tempTextList.get(7);
						String amount_actual = tempTextList.get(9).replaceAll(".00", "");

						// get expected data
						String name_expected = testData.get(Constants.PAYEE_NAME + "_1");
						;

						String transaction_type_expected = "";
						String amount_expected = "";
						if (int1 > 0) {
							transaction_type_expected = "Debit";
							amount_expected = "$" + String.valueOf(int1);
						} else {
							transaction_type_expected = "Credit";
							amount_expected = "$" + String.valueOf(int2);
						}

						// verify transaction id should be same for both users
						if (testData.containsKey(Constants.TRANSACTION_ID)) {
							String transaction_ID_expected = tempTextList.get(1);

							if (!transaction_ID_expected.equals(transaction_ID))
								softAssertAndTakeScreenshot(
										"Transcation Id not correct for both users - First User :"
												+ transaction_ID_expected + ", Second User:" + transaction_ID,
										testcaseName, true);
							else
								addLogAndExtentReportMessage("Transcation Id  correct for both users");

						} else {

							testData.put(Constants.TRANSACTION_ID, tempTextList.get(1));

						}

						// verify for name
						if (!name_expected.equals(name_actual))
							softAssertAndTakeScreenshot(
									"Payee name not correct on Transaction details page - Expected :" + name_expected
											+ ", Actual:" + name_actual,
									testcaseName, true);
						else
							addLogAndExtentReportMessage("Payee name correct on Transaction details page");

						// verify for amount

						if (!amount_expected.equals(amount_actual))
							softAssertAndTakeScreenshot("Amount not correct on Transaction details page - Expected :"
									+ amount_expected + ", Actual:" + amount_actual, testcaseName, true);
						else
							addLogAndExtentReportMessage("Amount correct on Transaction details page");

						// verify for transaction type
						if (!transaction_type_expected.equals(transaction_type_actual))
							softAssertAndTakeScreenshot(
									"Transaction Type not correct on Transaction details page - Expected :"
											+ transaction_type_expected + ", Actual:" + transaction_type_actual,
									testcaseName, true);
						else
							addLogAndExtentReportMessage("Transaction Type correct on Transaction details page");

					} else
						softAssertAndTakeScreenshot("User is not on transaction details view page. ", testcaseName,
								true);

				} else
					softAssertAndTakeScreenshot("User is not on account details view page. ", testcaseName, true);

			} else
				softAssertAndTakeScreenshot("User is not on account over view page. ", testcaseName, true);

		} catch (Exception e) {
			softAssertAndTakeScreenshot("Exception occured->" + e.getMessage(), testcaseName, true);
			assertAll();
		}
	}

	@Then("I login with first user again")
	public void i_login_with_first_user_again() {
		try {
			String userName = testData.get(Constants.API_USERNAME + "_1");
			String password = testData.get(Constants.API_PASSWORD + "_1");
			addLogAndExtentReportMessage("Attempting login with :-->" + userName + " \\ " + password);
			JAVAMethods.login(userName, password);
		} catch (Exception e) {
			softAssertAndTakeScreenshot("Exception occured->" + e.getMessage(), testcaseName, true);
			assertAll();
		}
	}

	@Then("I assert all")
	public void i_assert_all() {
		assertAll();
	}
}
