package Constants;


public class Paths {

	/**
	 * This class contains Paths which will be used in framework
	 */
	public static  final String PATH_ROOT= System.getProperty("user.dir");
	public static final String PATH_PROPERTIES_CONFIG=PATH_ROOT+"/config/config.properties";
	public static final String PATH_LOG_FILE=PATH_ROOT+"/Logs/Logs.log";
	public static final String PATH_EXTENT_REPORT=PATH_ROOT+"/CurrentTestResults/ExtentReportResults.html";
	public static final String PATH_EXTENT_REPORT_SCREENSHOT=PATH_ROOT+"/CurrentTestResults/Screenshots";
	public static final String PATH_CURRENT_TEST_RESULTS=PATH_ROOT+"/CurrentTestResults";
	public static final String PATH_ARCHIVED_TEST_RESULTS=PATH_ROOT+"/ArchivedTestResultsAndLogs";
	public static final String PATH_LOGS_FOLDER=PATH_ROOT+"/Logs";
 
	 
}
