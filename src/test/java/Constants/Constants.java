package Constants;

public class Constants {
	/**
	 * This class contains Constants which will be used in framework
	 */

	// Config constants
	public static final String CONFIG_BROWSER = "browser";
	public static final String CONFIG_URL = "URLforBank";
	public static final String CONFIG_MAX_RETRY = "maxRetryForChanginUserName";
	public static final String CONFIG_RANDOM_USER_API_URL = "randomeUserAPIURL";
	public static final String BROWSER_CHROME = "chrome";
	public static final String BROWSER_FIREFOX = "firefox";
	public static final String BROWSER_IE = "IE";

	// API setter method constants
	public static final String API_RESULTS = "results";
	public static final String API_GENDER = "gender";
	public static final String API_NAME = "name";
	public static final String API_TITLE = "title";
	public static final String API_FIRST = "first";
	public static final String API_LAST = "last";
	public static final String API_LOCATION = "location";
	public static final String API_STREET = "street";
	public static final String API_NUMBER = "number";
	public static final String API_CITY = "city";
	public static final String API_STATE = "state";
	public static final String API_COUNTRY = "country";
	public static final String API_POSTCODE = "postcode";
	public static final String API_EMAIL = "email";
	public static final String API_LOGIN = "login";
	public static final String API_UUID = "uuid";
	public static final String API_USERNAME = "username";
	public static final String API_PASSWORD = "password";
	public static final String API_DOB = "dob";
	public static final String API_DATE = "date";
	public static final String API_AGE = "age";
	public static final String API_REGISTERED = "registered";
	public static final String API_PHONE = "phone";
	public static final String API_CELL = "cell";
	public static final String API_ID = "id";
	public static final String API_VALUE = "value";
	public static final String API_PICTURE = "picture";
	public static final String API_LARGE = "large";
	public static final String API_MEDIUM = "medium";
	public static final String API_THUMBNAIL = "thumbnail";
	public static final String PAYEE_NAME = "payee name";
	public static final String ACCOUNT_NUMBER = "account number";
	public static final String TRANSACTION_ID = "transaction id";
	
	// Generic constants.
	public static final String DOUBLE_QUOTE = "\"";
	public static final String BLANK = "";
	public static final String CONNECTION_TIME_OUT="Connection timed out";
	
	

}