package Util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import Base.Base;
import Constants.Constants;

public class APIJSONParser extends Base {

	public static String gender = Constants.BLANK;
	public static String title = Constants.BLANK;
	public static String first = Constants.BLANK;
	public static String last = Constants.BLANK;
	public static String name = title + " " + first + " " + last;
	public static String street_number = Constants.BLANK;
	public static String street_name = Constants.BLANK;
	public static String city = Constants.BLANK;
	public static String state = Constants.BLANK;
	public static String country = Constants.BLANK;
	public static String postcode = Constants.BLANK;
	public static String email = Constants.BLANK;
	public static String uuid = Constants.BLANK;
	public static String username = Constants.BLANK;
	public static String password = Constants.BLANK;
	public static String dob_date = Constants.BLANK;
	public static String dob_age = Constants.BLANK;
	public static String registered_dob_date = Constants.BLANK;
	public static String registered_dob_age = Constants.BLANK;
	public static String phone = Constants.BLANK;
	public static String cell = Constants.BLANK;
	public static String id_name = Constants.BLANK;
	public static String id_value = Constants.BLANK;
	public static String picture_large = Constants.BLANK;
	public static String picture_medium = Constants.BLANK;
	public static String picture_thumbnail = Constants.BLANK;

	/**
	 * @author yogendrarathore
	 * @throws Exception
	 */
	public static void setValuesForAPIVariables() throws Exception {
		
		RestAssuredAPITestMethods.getResponse(props.getProperty(Constants.CONFIG_RANDOM_USER_API_URL));

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(responseBody);

		JsonNode node1 = node.get(Constants.API_RESULTS).get(0);

		gender = node1.get(Constants.API_GENDER).toPrettyString().replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		title = node1.get(Constants.API_NAME).get(Constants.API_TITLE).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		first = node1.get(Constants.API_NAME).get(Constants.API_FIRST).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		last = node1.get(Constants.API_NAME).get(Constants.API_LAST).toPrettyString().replaceAll(Constants.DOUBLE_QUOTE,
				Constants.BLANK);
		name = title + " " + first + " " + last;
		street_number = node1.get(Constants.API_LOCATION).get(Constants.API_STREET).get(Constants.API_NUMBER)
				.toPrettyString().replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		street_name = node1.get(Constants.API_LOCATION).get(Constants.API_STREET).get(Constants.API_NAME)
				.toPrettyString().replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		city = node1.get(Constants.API_LOCATION).get(Constants.API_CITY).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		state = node1.get(Constants.API_LOCATION).get(Constants.API_STATE).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		country = node1.get(Constants.API_LOCATION).get(Constants.API_COUNTRY).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		postcode = node1.get(Constants.API_LOCATION).get(Constants.API_POSTCODE).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		email = node1.get(Constants.API_EMAIL).toPrettyString().replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		uuid = node1.get(Constants.API_LOGIN).get(Constants.API_UUID).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);

		// adding random number to username to avoid duplicate user name check.
		username = node1.get(Constants.API_LOGIN).get(Constants.API_USERNAME).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		password = node1.get(Constants.API_LOGIN).get(Constants.API_PASSWORD).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		dob_date = node1.get(Constants.API_DOB).get(Constants.API_DATE).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		dob_age = node1.get(Constants.API_DOB).get(Constants.API_AGE).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		registered_dob_date = node1.get(Constants.API_REGISTERED).get(Constants.API_DATE).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		registered_dob_age = node1.get(Constants.API_REGISTERED).get(Constants.API_AGE).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		phone = node1.get(Constants.API_PHONE).toPrettyString().replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		cell = node1.get(Constants.API_CELL).toPrettyString().replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		id_name = node1.get(Constants.API_ID).get(Constants.API_NAME).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		id_value = node1.get(Constants.API_ID).get(Constants.API_VALUE).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		picture_large = node1.get(Constants.API_PICTURE).get(Constants.API_LARGE).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		picture_medium = node1.get(Constants.API_PICTURE).get(Constants.API_MEDIUM).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		picture_thumbnail = node1.get(Constants.API_PICTURE).get(Constants.API_THUMBNAIL).toPrettyString()
				.replaceAll(Constants.DOUBLE_QUOTE, Constants.BLANK);
		
	 
		
		
		setValuesInTestData();
		
		
		 
	}

	/**
	 * @author yogendrarathore
	 * @throws Exception
	 */
	public static void setValuesInTestData() throws Exception {

		testDataIndex++;
		testData.put(Constants.PAYEE_NAME + "_" + testDataIndex, name);
		testData.put(Constants.API_STREET + "_" + testDataIndex, street_number + " " + street_name);
		testData.put(Constants.API_CITY + "_" + testDataIndex, city);
		testData.put(Constants.API_STATE + "_" + testDataIndex, state);
		testData.put(Constants.API_POSTCODE + "_" + testDataIndex, postcode);
		testData.put(Constants.API_PHONE + "_" + testDataIndex, phone);
		testData.put(Constants.API_USERNAME + "_" + testDataIndex, username);
		testData.put(Constants.API_PASSWORD + "_" + testDataIndex, password);

		System.out.println (testData.toString());
	}
}
