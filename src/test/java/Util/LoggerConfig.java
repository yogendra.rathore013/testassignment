package Util;

import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import Constants.Paths;

public class LoggerConfig {

	/**
	 * This class creates object of logger
	 */
	public static FileHandler fh = null;
	public static Logger logger= null;

	public static Logger setLoggerFilePath() {

		
		logger= Logger.getGlobal();
	 	
		try {
		 
			if (fh == null) {

				fh = new FileHandler(Paths.PATH_LOG_FILE);
				logger.addHandler(fh);
				
			 
				SimpleFormatter formatter = new SimpleFormatter();
				fh.setFormatter(formatter);
			
				}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return logger;
	}
	
	
}
