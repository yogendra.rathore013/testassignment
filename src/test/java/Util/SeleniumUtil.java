package Util;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Base.Base;

public class SeleniumUtil extends Base {
	
	
	
	/**
	 * THis class has all basic funcitons of selenium web driver
	 */
	public static Actions act = null;

	
	/** 
	 * This mehtod check xpath is displayed or not
	 * and retunrs true or false
	 */
	public static boolean checkXapthIsDisplayed(String xPath, int timeoutInSeconds) throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xPath))));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 
	 * @param xPath
	 * @return
	 * @throws Exception
	 */
	public static WebElement getElement(String xPath) throws Exception {

		if (checkXapthIsDisplayed(xPath, 5)) {
			return driver.findElement(By.xpath(xPath));
		}  
		return null;
	}

	/**
	 * 
	 * @param xPath
	 * @return
	 * @throws Exception
	 */
	public static List<WebElement> getElements(String xPath) throws Exception {
		if (checkXapthIsDisplayed(xPath, 5)) {
			return driver.findElements(By.xpath(xPath));
		}  
		return null;
	}

	
	/**
	 * 
	 * @param ele
	 * @return
	 * @throws Exception
	 */
	public static String getText(WebElement ele) throws Exception {

		return ele.getText().trim();
	}

	
	/**
	 * 
	 * @param xPath
	 * @return
	 * @throws Exception
	 */
	public static String getText(String xPath) throws Exception {

		return getElement(xPath).getText().trim();

	}
	
	/**
	 * 
	 * @param xPath
	 * @return
	 * @throws Exception
	 */
	public static List<String>   getTextsFromAllElements(String xPath) throws Exception {

		List<WebElement> temp= getElements(xPath);
		
		List<String> tempTextList= new ArrayList<String>();
		
		for(WebElement e : temp)
		{
			tempTextList.add(e.getText().trim());
		}
		return tempTextList;

	}

	/**
	 * 
	 * @param xPath
	 * @param value
	 * @throws Exception
	 */
	public static void enterText(String xPath, String value) throws Exception {

		if(value ==null)
		{
			addLogAndExtentReportMessage("Value sent to enter is null for xPath--> "+xPath);
		}
			else 
		if (checkXapthIsDisplayed(xPath, 5)) {
			WebElement ele = getElement(xPath);
			ele.click();
			ele.clear(); 
			ele.sendKeys(value);
		 
		} 
		SeleniumUtil.applicationWait(200);
	}
 
	/**
	 * 
	 * @param xPath
	 * @throws Exception
	 */
	public static void click(String xPath) throws Exception {

		if (checkXapthIsDisplayed(xPath, 5)) {
			getElement(xPath).click();

		}  
		SeleniumUtil.applicationWait(2000);
	}

	/**
	 * 
	 * @throws Exception
	 */
	public static void applicationWait(int timeInMilisecs) throws Exception {

		Thread.sleep(timeInMilisecs); 
	}


	 
}
