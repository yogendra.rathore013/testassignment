package Util;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import Base.Base;
import Constants.Constants;
import io.github.bonigarcia.wdm.WebDriverManager;

public class MyDriverManager extends Base {

 
	 
	/** 
	 * This class takes care of intializing driver as per user input of browser name
	 * @throws Exception
	 */
	public static void intializeBrowser() throws Exception {
		addLogAndExtentReportMessage("IntializeBrowser driver" );
		String browserName = props.getProperty(Constants.CONFIG_BROWSER);
		if(driver==null ) {
		addLogAndExtentReportMessage("Browser -" + browserName);
		if (browserName.equalsIgnoreCase(Constants.BROWSER_CHROME)) {
			WebDriverManager.chromedriver().setup();
			  
			driver= new ChromeDriver();
		} else

		if (browserName.equalsIgnoreCase(Constants.BROWSER_FIREFOX)) {
			WebDriverManager.firefoxdriver().setup();
			driver= new FirefoxDriver();
		}

		else

		if (browserName.equalsIgnoreCase(Constants.BROWSER_IE)) {
			WebDriverManager.iedriver().setup();
			
			//code to ignore IE zoom issue
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("ignoreZoomSetting", true);
			 ;
			driver= new InternetExplorerDriver(caps);
		}

		else
			throw new Exception("Browser Name not correct. -" + browserName);

		driver.manage().window().maximize();
		addLogAndExtentReportMessage("Maximized Browser" );
 		
		}
		

	} 

}
