package Util;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

import Base.Base;
import Constants.Paths;

public class ReadPropertiesFile extends Base {
	 
	/**
	 * This class read properties file and intialize properties object
	 * @throws Exception
	 */
	public ReadPropertiesFile() throws Exception {
		addLogAndExtentReportMessage("Reading Properties File to intialize Props object.");
		File configFile = new File(Paths.PATH_PROPERTIES_CONFIG);

		try {

			if (props == null) {
				FileReader reader = new FileReader(configFile);
				props = new Properties();
				props.load(reader);

			}
		} catch (Exception e) {
			 
			addLogAndExtentReportMessage("Exception in ReadPropertiesFile -" + e.getLocalizedMessage());
			throw new Exception("Exception in ReadPropertiesFile -" + e.getLocalizedMessage());
		}

	}
}
