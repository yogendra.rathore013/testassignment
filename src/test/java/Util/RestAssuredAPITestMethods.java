package Util;

import Base.Base;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class RestAssuredAPITestMethods extends Base {

	// --------------------------------------------------------------
	private static void setBaseURI(String URI) {
		RestAssured.baseURI = URI;
		addLogAndExtentReportMessage("Set Base URI -->" + URI);
	}

	// --------------------------------------------------------------
	private static void setRequest() {
		request = RestAssured.given();
		addLogAndExtentReportMessage("Request Generated ");

	}

	// --------------------------------------------------------------
	public static void getResponse(String URI) throws Exception {

		try {
			setBaseURI(URI);
			setRequest();
			response = request.request(Method.GET, "");

			responseBody = response.prettyPrint();

			responseStatus = response.getStatusCode();

		} catch (Exception e) {
			addLogAndExtentReportMessage("API down- Run test case again after some time.");
			throw new Exception("API down- Run test case again after some time.");
		}
	}

}
