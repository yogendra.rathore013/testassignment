package Util;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import Base.Base;
import Constants.Paths;

public class JAVAUtil extends Base {

	 /** 
	  * THis clas contains all java functions
	  */ 
	
    private static final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
 
    
    /**
     * THis method returns time stamp
     * @return
     */
    public static String getTimeStamp(){

          Timestamp timestamp = new Timestamp(System.currentTimeMillis());
       
      
       return sdf1.format(timestamp);        
 
    }
    
    
    /**
     * THis mehtod captures screenshot
     * @param testcaseName
     * @return
     */
	public static String captureScreenshot(String testcaseName) {

	 try {
		String ScreenshotPath = Paths.PATH_EXTENT_REPORT_SCREENSHOT;
	 
	 	File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String screenshotName = testcaseName + getTimeStamp() + ".png";
		String screenshotpath = ScreenshotPath + "/" + screenshotName;

		try {
			FileUtils.copyFile(src, new File(screenshotpath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "." + "/screenshots" + "/" + screenshotName;

	 }catch(Exception e)
	 {
		 e.printStackTrace();
	 }
	 
	 return null;
	}


	/**
	 * THis method backup test report and logs
	 */
	public static void backupTestReportAndLogs( ) { 
		try {
			addLogAndExtentReportMessage( "backupTestReportAndLogs");
		//create a folder with time stamp
			String folderName= "TestResults_"+ getTimeStamp();
			 
			createFolderAtGivenLocation(Paths.PATH_ARCHIVED_TEST_RESULTS, folderName);
		
		// copy logs in it
			moveFileFromSrcToDest(Paths.PATH_LOGS_FOLDER, Paths.PATH_ARCHIVED_TEST_RESULTS+"//"+ folderName);
		
		//copy screenshots adn extend report
			 moveFileFromSrcToDest(Paths.PATH_CURRENT_TEST_RESULTS, Paths.PATH_ARCHIVED_TEST_RESULTS+"//"+ folderName);
			
		} catch (Exception e) {
			addLogAndExtentReportMessage( e.getLocalizedMessage());

		}

	}

	
 /** 
  * This mehtod create folder at given location
  * @param location
  * @param folderName
  */

	public static void createFolderAtGivenLocation(String location, String folderName) {

		try {
			addLogAndExtentReportMessage("create Folder "+folderName+" At Given Location"+ location);

			final String srcFolder = location + "\\" + folderName;

			File theDir = new File(srcFolder);

			// if the directory does not exist, create it
			if (!theDir.exists())

				// make directory
				theDir.mkdir();

		} catch (Exception e) {
			addLogAndExtentReportMessage( e.getLocalizedMessage());

		}

	}
	
	
	/**
	 * This method check folder exists at given location
	 * @param location
	 * @param folderName
	 * @return
	 */

	public static boolean checkFolderExistsAtGivenLocation(String location, String folderName) {

		try {

			final String srcFolder = location + "\\" + folderName;

			File theDir = new File(srcFolder);

			// if the directory does not exist, create it
			return theDir.exists();

		} catch (Exception e) {
			addLogAndExtentReportMessage( e.getLocalizedMessage());

		}
		return false;

	}

	/**
	 * This method delete folder at given location
	 * @param location
	 * @param folderName
	 */
	public static void deleteFolderAtGivenLocation(String location, String folderName) {

		try {

			final String srcFolder = location + "\\" + folderName;

			File directory = new File(srcFolder);

			// make sure directory exists
			if (directory.exists())

				deleteFile(directory);
			else
				addLogAndExtentReportMessage( "Folder doesnt exist to delete :" + srcFolder);

		} catch (Exception e) {
			addLogAndExtentReportMessage( e.getLocalizedMessage());

		}

	}
	
	/**
	 * THis method deletes file
	 * @param file
	 * @throws IOException
	 */

	public static void deleteFile(File file) throws IOException {

		if (file.isDirectory()) {

			// directory is empty, then delete it
			if (file.list().length == 0) {

				file.delete();

			} else {

				// list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					// construct the file structure
					File fileDelete = new File(file, temp);

					// recursive delete
					deleteFile(fileDelete);
				}

				// check the directory again, if empty then delete it
				if (file.list().length == 0) {
					file.delete();

				}
			}

		} else {
			// if file, then delete it
			file.delete();

		}

	}
	
	/**
	 * THis mehtod method move file from src to dest
	 * @param source
	 * @param destination
	 */

	public static void moveFileFromSrcToDest(String source, String destination) {

		try {
		 	File src = new File(source);
			File dest = new File(destination);

			FileUtils.copyDirectory(src, dest);

		} catch (Exception e) {
			addLogAndExtentReportMessage( e.getLocalizedMessage());

		}

	}

	/**
	 * THis mehtod return random number
	 * @param source
	 * @param destination
	 */

	public static String getRandomNumberLessThanGivenNumber(int num) {
		int n = 0;
		try {

			Random rand = new Random();
			n = rand.nextInt(num);
			

		} catch (Exception e) {
			addLogAndExtentReportMessage( e.getLocalizedMessage());

		}
		return String.valueOf(n);

	}
	
;
	 
}
