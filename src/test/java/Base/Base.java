package Base;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import Constants.Constants;
import Constants.Paths;
import Util.JAVAUtil;
import Util.LoggerConfig;
import Util.MyDriverManager;
import Util.SeleniumUtil;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Base {

	/**
	 * THis Base Class contains all the common methods and Variables which are used
	 * throughout the project Concept of Inheritance is used to access all methods
	 * and variables of this class.
	 * 
	 * It was mostly thread local variables to make it thread safe during parallel
	 * execution.
	 */

	// public variables 
	public static WebDriver driver = null;
	public static String testcaseName = null;

	public static Properties props = null;
	// private variables 
	private static ExtentHtmlReporter htmlReporter = null;
	private static ExtentReports extent = null;
	private static SoftAssert softAssert = null;
	private static Logger logger = null;
	private static ExtentTest test = null;
	private static List<String> screenshots = new ArrayList<String>();
	public static RequestSpecification request = null;
	public static Response response = null;
	public static int responseStatus = 0;
	public static String responseBody ="";
	public static Hashtable <String,String> testData= new Hashtable<String,String>();
	public static int testDataIndex=0;
	/**
	 * @implNote This methods open browser and fetches the URL ( browser and URL are
	 *           fetched from config properties file).
	 * @throws Exception
	 */
	public void openHomePage() throws Exception {
		try {
 
			MyDriverManager.intializeBrowser(); 
			String url = props.getProperty(Constants.CONFIG_URL);
			 
			addLogAndExtentReportMessage(" Opening URL :" + url);
			driver.navigate().to(url);

			 
		} catch (Exception e) {
			softAssertAndTakeScreenshot("Not able to open Home Page,Exception in method. ", testcaseName, true);
			assertAll();
		}

	}

	/**
	 * This method add message to both Log file in Logs folder and extent report
	 * 
	 * @param message
	 */
	public static void addLogAndExtentReportMessage(String message) {
		System.out.println(message);
		if (test != null)
			test.log(Status.INFO, message);

		if (logger == null)
			logger = LoggerConfig.setLoggerFilePath();

		logger.info(message);
	}

	/**
	 * This method soft asserts the given message and take screenshot of the
	 * applicaiton. Then it adds the name of the screenshot to screenshot list.
	 */
	public void softAssertAndTakeScreenshot(String message, String testcaseName, boolean takeScreenshot) {
		
		addLogAndExtentReportMessage(message);
		softAssert.fail(message);

		if (takeScreenshot) {
			String screenshotPath = JAVAUtil.captureScreenshot(testcaseName);

			screenshots.add(screenshotPath);

		}
		
		assertAll();
	}

	/**
	 * This method clear soft assertion and screenshot list
	 */
	public void clearSoftAssertAndScreenshotsList() {
		
		softAssert=new SoftAssert();
		screenshots.clear();

		;
	}

	/**
	 * This method assert all to soft assert object
	 */
	public void assertAll() {
		 
		if (softAssert != null)
			softAssert.assertAll();
		;
	}

	/**
	 * This method create Extent report content based on test case status
	 * 
	 * @param result
	 */
	public void createStatusForExtentReport(String result) {
 
		if (result.equalsIgnoreCase("FAILED")) {
			//test.log(Status.FAIL, result.getThrowable());

			test.log(Status.WARNING, "Screenshot attached below, click on icon");

			test.log(Status.WARNING,
					"Screenshot can also be found in screenshots folder with name(s) listed below :-");
			String screenshotPath = "";

			for (int i = 0; i < screenshots.size(); i++) {
				try {
					screenshotPath = screenshots.get(i);
					test.log(Status.WARNING, screenshotPath);
					test.addScreenCaptureFromPath(screenshotPath);
				} catch (IOException e) {
					test.log(Status.ERROR, "Error in attaching screenshot.");
				}
			}
		} 
			else if (result.equalsIgnoreCase("PASSED")) {
			test.log(Status.PASS, result);
		} else {
			test.log(Status.SKIP, result);
		}

	}

	/**
	 * This method creates instance of Extent test object
	 */
	public void createTestForExtent(String scName) {

		testcaseName=scName;
		test=extent.createTest(testcaseName);

	}

	/**
	 * This method starts Extent Report by instancing ExtentHtmlReporter and
	 * ExtentReports
	 */
	public void startExtentReport() {
		if (htmlReporter == null) {
			htmlReporter = new ExtentHtmlReporter(Paths.PATH_EXTENT_REPORT);
			htmlReporter.config().setDocumentTitle("UNLIMINT Automation Report");
			htmlReporter.config().setReportName("UNLIMINT Automation Report");
			htmlReporter.config().setTheme(Theme.DARK);
			htmlReporter.config().setTimeStampFormat("EEEE, MMMM dd, yyyy, hh:mm a '('zzz')'");

			extent = new ExtentReports();

			extent.attachReporter(htmlReporter);
		}

	}

	/**
	 * This method flush extent report object
	 */
	public void flushExtentReport() {

		extent.flush();
		
		System.err.println("Check Extent Report in CurrentTestResults Folder");

	}

}
